﻿using static System.Console;

namespace interface_example
{
    public abstract class Cat
    {
        public abstract void Eat();

        public abstract void Hunt();

        public abstract void Sleep();

        private static void Main(string[] args)
        {
            Cheetah cheetah = new Cheetah();
            cheetah.Hunt();
            cheetah.Eat();
            cheetah.Sleep();
            cheetah.SoftPurr(60);
            ReadLine();
        }
    }

    public class Cheetah : Cat, IPurrable
    {
        //public int SoftPurr { get; set; }

        public override void Eat()
        {
            WriteLine($"The cheetah eats.");
        }

        public override void Hunt()
        {
            WriteLine($"The cheetah Hunts.");
        }

        public override void Sleep()
        {
            WriteLine($"The cheetah sleeps.");
        }

        public void SoftPurr(int decibel)
        {
            WriteLine($"The {nameof(Cheetah)} purrs at {decibel} decibels.");
        }
    }

    internal interface IPurrable
    {
        void SoftPurr(int decibel);
    }
}