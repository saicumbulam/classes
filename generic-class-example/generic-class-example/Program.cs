﻿using static System.Console;

namespace generic_class_example
{
    internal class Program
    {
        public enum MyEnum { Value1, Value2, Value3 }
        private static void Main(string[] args)
        {

        PerformAction<int> iAction = new PerformAction<int>(21);
            iAction.IdentifyDataType();

            PerformAction<decimal> dAction = new PerformAction<decimal>
(21.55m);
            dAction.IdentifyDataType();

            PerformAction<string> sAction = new PerformAction<string>
("Hello Generics");
            sAction.IdentifyDataType();

            MyHelperClass oHelper = new MyHelperClass();
            var intExample = oHelper.InspectType(25);

            WriteLine($"An example of this type is {intExample}");
            var decExample = oHelper.InspectType(11.78m);

            WriteLine($"An example of this type is {decExample}");
            var strExample = oHelper.InspectType("Hello Generics");

            WriteLine($"An example of this type is {strExample}");
            var enmExample = oHelper.InspectType(MyEnum.Value2);

            WriteLine($"An example of this type is {enmExample}");

            ReadLine();
        }
    }

    public class PerformAction<T>
    {
        private T _value;

        public PerformAction(T value)
        {
            _value = value;
        }

        public void IdentifyDataType()
        {
            WriteLine($"The data type of the supplied variable is { _value.GetType()}");
        }


    }

    public class MyHelperClass
    {
        public T InspectType<T>(T value)
        {
            WriteLine($"The data type of the supplied parameter is{ value.GetType()}");
            return (T)value;
        }
    }
}