﻿using System;
using static System.Console;

namespace abstract_class_example
{
    public abstract class Cat
    {
        public abstract void Eat();

        public abstract void Hunt();

        public abstract void Sleep();
        private static void Main(string[] args)
        {
            Lion lion = new Lion(Lion.ColorSpectrum.White);
            lion.Hunt();
            lion.Eat();
            lion.Sleep();
            Tiger tiger = new Tiger(Tiger.ColorSpectrum.Blue);
            tiger.Hunt();
            tiger.Eat();
            tiger.Sleep();
            Console.ReadLine();
        }
    }

    public class Lion : Cat
    {
        public enum ColorSpectrum { Brown, White }
        public string LionColor { get; set; }
        public override void Eat()
        {
            WriteLine($"The {LionColor} lion eats.");
        }

        public override void Hunt()
        {
            WriteLine($"The {LionColor} lion hunts.");
        }

        public override void Sleep()
        {
            WriteLine($"The {LionColor} lion sleeps.");
        }

        public Lion(ColorSpectrum color)
        {
            LionColor = color.ToString();
        }
    }
    public class Tiger : Cat
    {
        public enum ColorSpectrum { Orange, White, Gold, Blue, Black }
        public string TigerColor { get; set; }
        public override void Eat()
        {
            WriteLine($"The {TigerColor} tiger eats.");
        }
        public override void Hunt()
        {
            WriteLine($"The {TigerColor} tiger hunts.");
        }
        public override void Sleep()
        {
            WriteLine($"The {TigerColor} tiger sleeps.");
        }
        public Tiger(ColorSpectrum color)
        {
            TigerColor = color.ToString();
        }
    }
}